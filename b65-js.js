// small root parser function
// v: 2015-03-23: added icon
//

var b65domain = "b65stockholm.se" ; 
var rootDir   = "area" ;
var linker    = "linker.htm" ;
var command   = "visa" ;
var keys      = ["all","ansokan","hb","forkort","stadgar","program","protokoll","matrikel","kvartalsbladet"];
var pageinfo  = ["doValidate","isLoggedIn"];
var oddfellow = 'http://oddfellow.se/hitta/institution/?inst=b65';
var oddfellow = 'http://b65stockholm/jubel';
var b65Name   = "ODD FELLOWLOGEN NR 65 STOCKHOLM I. O. O. F. AV SVERIGE";
var b65Logo   = "http://oddfellow.se/uploads_old/65/1313943329_b65.jpg";
var OFLogo    = "http://10viktorrydberg.org/onewebstatic/762ed5754f-3lankar.jpg";
var b65ico    = "https://onedrive.live.com/download.aspx?cid=680C245ED9FA1242&resid=680C245ED9FA1242%21330&authkey=%21AGioK7BXmzlG7LU&canary=gAb3nL%2FF%2B8hZdfp8n2MFxHNJyPt5LTl0ZkhCvdZKii8%3D2";

function sleep(milliseconds) 
{
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}
// The next function does not work, ico is in index.html at server
function setIcon()
{
	var att = document.createAttribute("link");
	att.rel ="icon";
	att.type="image/png";
	att.href= b65ico;
}

function showLodgeHeader(box){
	setIcon();

    document.getElementById(box).innerHTML = "<table width=430 align=center class=ansokanTop><tr><td><img src=" + b65Logo + "></td><td><center><img width=123 src=" + OFLogo + "><br><b>" + b65Name + "</b></center></td>";	
}

function showLodgeCopy(box){
	setIcon();
    document.getElementById(box).innerHTML = "<div><center><b>&copy;&nbsp;" + b65Name + "</b></center></div>";	
}

function showLodge(box){
	setIcon();
    document.getElementById(box).innerHTML = "<div class=logo><center><img src=" + b65Logo + "><br><b>&copy;&nbsp;" + b65Name + "</b></center></div>";	
}
function setParsedTitle()
{
    document.title = "B65 Stockholm: " + document.formMall.knapp.value.toString().trim();
    showLodge('referBox');
}

function beginsWith(a,b)
{
	return a.substr(0,b.length) == b ;
}

function loggedInShowDoc(pageRef)
{
	// k = "protokoll";
	var myUrl="http://" + b65domain + assembleCommand(pageRef);
	var myUrl="http://" + b65domain;

	//document.getElementById('testBox').innerHTML += "<li>page: " + page;
	if (!beginsWith(document.referrer.toString(),myUrl)) 
		printError(401,'errBox',"<br>" + myUrl + "<br>Direktadress funkar inte, du ska logga in.<br>" + document.referrer);
//	document.getElementById('testBox').innerHTML += "<li> :loggedInShowDoc:" + key + " =[" + key + "]" ;
	var out = getDocByKey(pageRef);
	//document.getElementById('mainBox').innerHTML = "Snart kommer ditt dokument... (" + pageRef + ") ("+out+")";
	cleanPage(['referBox','testBox']);

	window.location = out;
	//document.getElementById('testBox').innerHTML += "<li> :loggedInShowDoc: foo =[" + foo + "]" ;
	// set window.loc=foo
	return 200;
}

function reLink()
{
	window.location = "http://" + b65domain + assembleCommand(window.location.pathname.split('/')[1]);
}


function parseURL(info)
{
	var ok       = 200;
	// var sep      = "/";
	var rD       = "/" + rootDir + "/" ;
	var location = document.location;
	var url      = location.toString();
	var queryStr = location.search.substring(1);
	// document.getElementById('testBox').innerHTML += "<li>"+ key;
	// if rootDir is at beginning of url.pathname => we are logged in via password
	// document.getElementById('testBox').innerHTML += "<li>hostname    = " + location.hostname.toString() ;
	//document.getElementById('testBox').innerHTML += "<li>pathname    = " + location.pathname.toString() ;
	//document.getElementById('testBox').innerHTML += "<li>"+location.pathname+" beginsWith "+rD+" is " + beginsWith(location.pathname,rD) ;

	// pre checks
	if (queryStr == "") return 0;
	if (pageinfo.indexOf(info) < 0) return printError(405,'errBox',"Oups, detta funkar inte!");
	//if      (url.indexOf(sep)  > -1)
	// return printError(400,'errBox',"Oups, felaktig separator i url "+url);
	var keyValues = queryStr.split("&"); // todo: kolla om det finns fler, och ge fel
	if (keyValues.length == 0) 
		return printError(406,'errBox',"Ange vad som skall visas" + url);
	if (keyValues.length > 1) 
		return printError(406,'errBox',"Bara ett inledande kommando fungerar." + url);

	var validKey = keyValues[0].split("=");

	if (validKey.length < 2) 
		return printError(406,'errBox',"Ange korrekt kommando som skall visas, " + url);
	
	var key         = validKey[0];
	var pageRef     = validKey[1];
	if (location.hash != "") pageRef += location.hash.toString();
	var page        = pageRef.split("#");

	//document.getElementById('testBox').innerHTML += "<li>page  = " + page ;

// 	if (url.indexOf(sep)  > -1 && (key == "" || keys.indexOf(key) < 0)) 
 	if (key != command) 
 		return printError(406,'errBox',"Oups, du angav ogiltig typ av kommando \'" + key + "\'");
 	if ( keys.indexOf(page[0]) < 0 ) 
 		return printError(406,'errBox',"Oups, du angav ogiltig typ av sida \'" + page[0] + "\', prova med en av " + keys);

	if ( info == pageinfo[1] && location.hostname.toString() == b65domain && beginsWith(location.pathname,rD)) {
		document.getElementById('mainBox').innerHTML += "<li><b>Vi befinner oss i den inloggade delen.</b>" ;
		return loggedInShowDoc(pageRef);
	}
	else {
	    if (info == pageinfo[0] ) {
	    	document.getElementById('mainBox').innerHTML = '<center><hr><font class=losen><B>Ange l&ouml;sen</B></font><form method=post action="javascript:checkPassword(\''+ pageRef +'\');" name=formMall><input type=password name=passOrd size=10><input name=knapp class=mButt type=submit value="Visa ' + page[0] + '"></form></center><hr>';
	    	setParsedTitle();
	    	return ok;
		}
		return 0;
	}
	return printError(501,'errBox',"Huh?!? Detta gick ju inte bra alls, med url " + url);
}

function checkIncoming(info)
{
	if (!parseURL(info))
		window.location = oddfellow;
}	

function assembleCommand(p)
{
	return "?" + command + "=" + p;
}

function checkPassword(pageRef)
{
    if (document.formMall.passOrd.value != "") {
    	check =  rootDir + "/" + escape(document.formMall.passOrd.value.toLowerCase()) + "/" + linker + assembleCommand(pageRef);
		// document.getElementById('testBox').innerHTML = "<hr><li>"+ check + "<hr>";
		try {
				top.location = check;			
		}
		catch (err) {
			document.body += "<hr>Error " + err + "<hr>";
		}
		finally {
			// add 404 exception catch
			document.formMall.passOrd.value = "";
		}
    } 
}
function cleanPage(boxList)
{
	for (d in boxList)
		document.getElementById(boxList[d]).innerHTML = "";
}

function printError(errcode,id,msg)
{
	try {
		throw new Error({'ReferenceError':'msg'});
	}
	catch(err) {
		document.getElementById(id).innerHTML += "<font color=red><b>"+errcode+"</b> Hoppsan, det blev fel: "+msg+"</font>";
		showLodge('referBox');
		cleanPage(['mainBox','referBox','testBox']);
    }
    finally {
    	document.title = errcode + " Finnes ej: " + msg;	
    	throw errcode;
    	return errcode;
    }
}