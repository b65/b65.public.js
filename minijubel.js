var minijubel=[[2017,'<hr><div class="headerBox" id="headerBox" class="mainBox"> </div><table align=center class="minijubel" id="minijubel"> <tr><!--td align=center>&#x22D8; &#x212C;65 &#x22D9;</td></tr> <td--> <tr><td id=jubelDatum>2017-06-10<td></tr><tr><td id=jubelDagen><td></tr><tr><td id=jubelKontakt><hr><a href=mailto:100arsjubel@b65stockholm.se title="Kontakt, idéer, förslag, synpunkter, frågor? Vi vill veta vad du kan hjälpa till med och vi behöver alla vara med! Hör av dig!">100arsjubel@b65stockholm.se</a><br>&nbsp;</td></tr><!--tr><td align=center>&#x22D8; &#x212C;65 &#x22D9;</td></tr>  <tr><td align=center><h1>&sdot;&sdot;&sdot; &#x2620; &sdot;&sdot;&sdot;</h1></td></--tr> </table>']];

function showMiniJubel(headerBox,ansokanBox,referBox)
{    
  showLodgeHeader(headerBox);    
  document.getElementById(ansokanBox).innerHTML = minijubel[0][1];
  document.title = "B65 Stockholm: 100-årsrjubel";
  showLodgeCopy(referBox);
}


function countdown(yr,mon,d,h,m,pastmessage,target){
 //yr,mon,d,h,m are the Year, Month, Day, Hour, and Minute of the date the future event occurs
 //pastmessage is the message to display when the event has already happened
 //target is the target element to write the output to
 var today=new Date();
 var futuredate=new Date(yr,mon-1,d,h,m); //don't care why but the month has to be decremented
 dd=futuredate-today; //this should be the difference in the two dates in milliseconds
 dday=Math.floor(dd/(60*60*1000*24)*1); //find the number of days in dd
 dhour=Math.floor((dd%(60*60*1000*24))/(60*60*1000)*1); //find the number of hours in dd
 dmin=Math.floor(((dd%(60*60*1000*24))%(60*60*1000))/(60*1000)*1); //find the number of minutes in dd
 var wholedmin=dmin;if (dmin < 10){wholedmin="0"+dmin;} //convert to 2 digits if necessary
 dsec=Math.floor((((dd%(60*60*1000*24))%(60*60*1000))%(60*1000))/1000*1); //find the number of seconds in dd
 var wholedsec=dsec;if (dsec < 10){wholedsec="0"+dsec;} //convert to 2 digits if necessary
 if(futuredate<=today){
  document.getElementById(target).innerHTML=pastmessage; //write this if the date is past
 }
 else {
  document.getElementById(target).innerHTML= dday+ " dagar, "+ dhour +" timmar, "+ dmin +" minuter och ett par " + dsec + " sekunder kvar..."; //write this is futuredate is in the future
}}

var jubelDagen = setInterval("countdown(2017,06,10,15,00,'Grattis till 100 &auml;r','jubelDagen')",1000); //this sets the drwho H1 element refreshing

//</script>