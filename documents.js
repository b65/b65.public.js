// thords 2015-02
// newest at top: datum-id, url
// last entry with 1917 is to always have a known last entry, bogus but controlled
var b65=[
	["hb",[
			 // [yyyy,"url"]
			["2015-01-01","http://oddfellow.se/2b/uploads/2211/hb-loger2015_ver_500.pdf"],
			["2013-11-27","https://onedrive.live.com/redir?resid=680c245ed9fa1242!338&authkey=!ANGkWaLjb2mE6co&ithint=file%2cpdf"],
		        ["1917-11-27",oddfellow]]],
	["all",[
			 // [yyyy,"url"]
			["2016-10-01","http://oddfellow.se/2b/uploads/3087/all_2016.pdf"],
			["2015-01-01","http://oddfellow.se/2b/uploads/1960/all_2015-01-01_v502.pdf"],
			["2014-04-18","https://onedrive.live.com/redir?resid=680c245ed9fa1242!337&authkey=!ANb9mNkeVR-x9s0&ithint=file%2cpdf"],
		        [1917,oddfellow]]],
	["forkort",[
			 // [yyyy,"url"]
			["2013-11-27","https://onedrive.live.com/redir?resid=680c245ed9fa1242!341&authkey=!ANIvZxOwQdGjW6c&ithint=file%2cpdf"],
		        [1917,oddfellow]]],
	["stadgar",[
			 // [yyyy,"url"]
			 	[2015,"https://1drv.ms/b/s!AkIS-tleJAxohE0h8DHnP_id7QRD"],
				//[2015,"https://onedrive.live.com/redir?resid=680C245ED9FA1242!362&authkey=!AL3P27CdYLv_CTM&ithint=file%2cpdf"],
		        [1995,"https://onedrive.live.com/redir?resid=680C245ED9FA1242!134&authkey=!AE6BWzoRRu62_d8&ithint=file%2cpdf"],
		        [1917,oddfellow]]],
	["program",[
			 // [yyyy,"url"]
	                [2017,"https://1drv.ms/b/s!AkIS-tleJAxohEQP2JyJvK44nP35"],
			[2016,"https://onedrive.live.com/redir?resid=680C245ED9FA1242!478&authkey=!AKWlXV9SfHvybSM&ithint=file%2cpdf"],
			[2015,"https://onedrive.live.com/redir?resid=680C245ED9FA1242!112&authkey=!AIZicXkLE1tcAOI&ithint=file%2cpdf"],
		        [2014,"https://onedrive.live.com/redir?resid=680C245ED9FA1242!126&authkey=!ACynR6BF9_pvdpE&ithint=file%2cpdf"],
		        [1917,oddfellow]]],
	["protokoll",[
		  	   // ["yyyy-mm-dd","url"]
	                ["2017-01-11","https://1drv.ms/b/s!AkIS-tleJAxohEe3csdXNc-_r6hO"],
	                ["2016-05-25","https://onedrive.live.com/redir?resid=680C245ED9FA1242!510&authkey=!AEgLfJdDy5hdlyE&ithint=file%2cpdf"],
	                ["2016-05-11","https://onedrive.live.com/redir?resid=680C245ED9FA1242!497&authkey=!AFefxI7YYnw2DoA&ithint=file%2cpdf"],
	                ["2016-04-27","https://onedrive.live.com/redir?resid=680C245ED9FA1242!498&authkey=!ADtZVuoGKYcTUgw&ithint=file%2cpdf"],
	                ["2016-04-13","https://onedrive.live.com/redir?resid=680C245ED9FA1242!500&authkey=!ADbInfYaewkvFbw&ithint=file%2cpdf"],
	                ["2016-03-23","https://onedrive.live.com/redir?resid=680C245ED9FA1242!499&authkey=!AOBtlg3xSE7FUpo&ithint=file%2cpdf"],
	                ["2016-03-09","https://onedrive.live.com/redir?resid=680C245ED9FA1242!501&authkey=!ADV8El0MHgkEWCU&ithint=file%2cpdf"],
	                ["2016-02-24","https://onedrive.live.com/redir?resid=680C245ED9FA1242!495&authkey=!AJvw01FTjwJCKh8&ithint=file%2cpdf"],
	                ["2016-02-10","https://onedrive.live.com/redir?resid=680C245ED9FA1242!489&authkey=!ALcgR4zfCISxX0o&ithint=file%2cpdf"],
	                ["2016-01-27","https://onedrive.live.com/redir?resid=680C245ED9FA1242!491&authkey=!AIAnCE2iDas3R7s&ithint=file%2cpdf"],
	                ["2016-01-13","https://onedrive.live.com/redir?resid=680C245ED9FA1242!494&authkey=!AHg8YcgCInHIq24&ithint=file%2cpdf"],
			["2015-05-13","https://onedrive.live.com/redir?resid=680c245ed9fa1242!345&authkey=!AM-aCpCgN_wq8lQ&ithint=file%2cpdf"],
			["2015-04-22","https://onedrive.live.com/redir?resid=680c245ed9fa1242!340&authkey=!AKBlEyd2UD5za7c&ithint=file%2cpdf"],
			["2015-04-08","https://onedrive.live.com/redir?resid=680c245ed9fa1242!332&authkey=!AJq8hkN4cWpz1QE&ithint=file%2cpdf"],
			["2015-03-25","https://onedrive.live.com/redir?resid=680c245ed9fa1242!333&authkey=!AChi6UKbdoqDgKc&ithint=file%2cpdf"],
		  	["2015-03-11","https://onedrive.live.com/redir?resid=680C245ED9FA1242!314&authkey=!AAqm4GHe0a8fxMs&ithint=file%2cpdf"],
		  	["2015-02-25","https://onedrive.live.com/redir?resid=680C245ED9FA1242!135&authkey=!ACFtLpBbXlLCxfA&ithint=file%2cpdf"], 
		  	["2015-02-11","https://onedrive.live.com/redir?resid=680C245ED9FA1242!138&authkey=!AGRAAykfg8A45hk&ithint=file%2cpdf"],
   			["2015-01-28","https://onedrive.live.com/redir?resid=680C245ED9FA1242!123&authkey=!AHA9OdN0I26V7JY&ithint=file%2cpdf"],
			["2015-01-14","https://onedrive.live.com/redir?resid=680C245ED9FA1242!122&authkey=!AESOcCj54FNn_p8&ithint=file%2cpdf"],
			["2014-11-26","https://onedrive.live.com/redir?resid=680C245ED9FA1242!113&authkey=!ANH-0NzsnX4uo0E&ithint=file%2cpdf"],
			["1917-06-02",oddfellow]]],			
	["matrikel",[
			// ["yyyy-mm","url"]
			["2015","https://onedrive.live.com/redir?resid=680C245ED9FA1242!347&authkey=!AP9hoF2b2WukEf8&ithint=file%2cpdf"],
			["1917-1",oddfellow]]],
	["kvartalsbladet",[
			// ["yyyy-mm","url"]
	        ["2017-1","https://1drv.ms/b/s!AkIS-tleJAxohEtErvB-J7w0yFln"],
	        ["2016-4","https://1drv.ms/b/s!AkIS-tleJAxohAiOBm7cfdL3YArz"],
			["2016-3","https://1drv.ms/b/s!AkIS-tleJAxog38E6MmfXMML7T9s"],
			["2016-2","https://onedrive.live.com/redir?resid=680C245ED9FA1242!504&authkey=!AAIcsZPGp5rEw0U&ithint=file%2cpdf"],
			["2016-1","https://onedrive.live.com/redir?resid=680C245ED9FA1242!505&authkey=!AMv7Kusp2i-_mo0&ithint=file%2cpdf"],
			["2015-4","https://onedrive.live.com/redir?resid=680C245ED9FA1242!509&authkey=!AAImQq4DapUyl9k&ithint=file%2cpdf"],
			["2015-3","https://onedrive.live.com/redir?resid=680C245ED9FA1242!508&authkey=!AJaBjP_I3_6f02Y&ithint=file%2cpdf"],
			["2015-2","https://onedrive.live.com/redir?resid=680C245ED9FA1242!507&authkey=!ACPEFA7gaV9935s&ithint=file%2cpdf"],
			["2015-1","https://onedrive.live.com/redir?resid=680C245ED9FA1242!506&authkey=!ALzGFalB52Y-DrU&ithint=file%2cpdf"],
			["1917-1",oddfellow]]]
	];

//function validKeys()
//{
//	var keys=[];
//	for (i in b65)
//		keys.concat([b65[i][0]]);
//	return keys;
//}
//
// Funktionen nedan plockar ut url delen, baserat på key, eller key#datum
// giltig input: program, protokoll, kvartalsbladet
// giltig input: program#2014, protokoll#2015-02-11, kvartalsbladet#2015-02
//

function getDocByKey(page)
{
	var errcode = 404;
	var keys = page.split("#");
	var datum = "";
	if (keys.length > 1) datum = keys[1];
	document.getElementById('testBox').innerHTML += "<hr>";
	for (o in b65){		
		if (b65[o][0] == keys[0]) {
			var x=-1;
			if ( keys.length>1 ) {
				for (d in b65[o][1]) 
					if (b65[o][1][d][0] == datum) 
						x = d ;
				if (x == -1) {
					return printError(errcode,'errBox',"det datumet " + datum + " finns inte f&ouml;r " + keys[0]);
				}
			}
			else x=0;
			return b65[o][1][x][1];
		}
	}
	return printError(501,'errBox',"ogiltig/felstavad dokumenttyp, " + keys[0]);
}